import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ThemeComponent } from './theme/theme.component';
import { LayoutModule } from './theme/layouts/layout.module';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ScriptLoaderService } from "./_services/script-loader.service";
import { ThemeRoutingModule } from "./theme/theme-routing.module";
import { AuthModule } from "./auth/auth.module";

import { RestangularModule, Restangular } from 'ngx-restangular';

import { StockService } from "./theme/pages/default/inner/_services/stock.service";

// Function for setting the default restangular configuration
export function RestangularConfigFactory (RestangularProvider) {
    RestangularProvider.setBaseUrl('http://localhost:8100');
    RestangularProvider.setDefaultHeaders({'Content-Type': 'application/json'});

    RestangularProvider.addFullRequestInterceptor((element, operation, path, url, headers, params) => {
        // console.log(params);
        // console.log("request Interceptor message");
        // console.log(element);
        // console.log(headers);
    });
}

@NgModule({
    declarations: [
        ThemeComponent,
        AppComponent,
    ],
    imports: [
        LayoutModule,
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        ThemeRoutingModule,
        AuthModule,
        HttpClientModule,
        // Importing RestangularModule and making default configs for restanglar
        RestangularModule.forRoot(RestangularConfigFactory)
    ],
    providers: [ScriptLoaderService, StockService],
    bootstrap: [AppComponent]
})
export class AppModule { }