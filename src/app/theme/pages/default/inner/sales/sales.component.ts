import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { BillingService } from "../_services/billing.service";

import { Subscription } from 'rxjs';
import {ItemService} from "../_services/item.service";

declare let swal: any;
declare let toastr: any;
declare let $: any;

@Component({
    selector: 'app-proforma',
    templateUrl: "sales.component.html",
    styleUrls: ["./sales.component.css"]
})
export class SalesComponent implements OnInit {

    // ~ paging variables ~ //
    private offset : number = 0;
    private limit_rows : number = 15;
    private no_rows_to_load :number = 7;

    // ~ proforma to be printed data ~ //
    printSaleItems : any = [];
    private printSaleDetail : any = {};

    busy: Subscription; // busy loading

    showDateRangeClearBtn : boolean = false;
    proformas: any;
    daterange: any = {};
    public dataSource;
    proformaDisplayedColumns = ['Id', 'Bill_To', 'Address', 'Proforma_Date', 'Amount', 'Actions'];

    constructor(private itemService: ItemService,private billing_service: BillingService) {
        this.busy = this.billing_service.getAllSales(this.offset, this.limit_rows).subscribe(res => {
            this.proformas = res;
            this.dataSource = new MatTableDataSource(this.proformas);
        }, errRes => {
            toastr.error('Can not load Sales');
        });
    }

    ngOnInit() {

    }


    applyFilter(filterValue: string) {
        if(filterValue.length == 0){
            // limit will be changed later.
            this.busy = this.billing_service.getAllSales(0, 15).subscribe(res => {
                this.proformas = res;
                this.dataSource = new MatTableDataSource(this.proformas);
            }, errRes => {
                toastr.error('Can not load proformas');
            });
            return;
        }

        if(filterValue.length < 2) return;

        this.busy = this.billing_service.searchSaleWithKeyword(filterValue).subscribe( res => {
            this.proformas = res;
            this.dataSource = new MatTableDataSource(this.proformas);
        }, errRes => {
            toastr.error('Something went wrong while searching table');
        });
    }

    showConfirmation_Remove_proforma_Swal(event) {
        let target = event.target || event.srcElement || event.currentTarget;
        let __customer__ = target.name;
        let __proforma_id_ = target.id;


        swal({
            title: "Sale to " + __customer__ + " is about to be removed !",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {

                    let _item_to_be_deleted = this.proformas.find(x => x.id == __proforma_id_);
                    if (_item_to_be_deleted != null) {
                        BillingService.delete(_item_to_be_deleted).subscribe(res => {
                            this.updateList();
                            toastr.success("Sale deleted !");
                        }, err => {
                            swal(err.data['message'], {
                                icon: "error",
                            });
                        });
                    }
                }
            });
    };

    selectWithDateRange(value: any) {
        this.showDateRangeClearBtn = true;

        this.daterange.start = value.start.format("YYYY-MM-DD");
        this.daterange.end = value.end.format("YYYY-MM-DD");

        this.billing_service.searchSaleByDateRange(this.daterange.start, this.daterange.end).subscribe(res => {
            this.proformas = res;
            this.dataSource = new MatTableDataSource(this.proformas);
        }, err => {
            toastr.error("Something went wrong");
            console.log(err);
        });
    }


    dateRangeClear(){
        this.showDateRangeClearBtn = false;
        // ~ _limit is the first numbers of row to load ~ //
        this.billing_service.getAllSales(0, 15).subscribe(res => {
            this.proformas = res;
            this.dataSource = new MatTableDataSource(this.proformas);
        }, errRes => {
            toastr.error('Can not load proformas');
            console.log(errRes);
        });
    }

    updateList(){
        // ~ update no rows to load ~ //
        this.busy = this.billing_service.getAllSales(0, 15).subscribe(res => {
            this.proformas = res;
            this.dataSource = new MatTableDataSource(this.proformas);
        }, errRes => {
            toastr.error('Can not load Sales');
        });
    }

    // ~ proforma is selected so print view should be generated ~ //
    sale_clicked(sale_Data) {

        this.printSaleDetail = sale_Data;
        this.printSaleItems = [];
        this.busy = this.billing_service.getItemOfSaleBill(sale_Data.id).subscribe( res => {
            console.log(res);
            this.printSaleItems = res.map( i => i);
            this.printSaleDetail.subTotal = 0;
            for (let i = 0; i < this.printSaleItems.length ; i++){
                // ~ calculating subtotal ~ //
                this.printSaleDetail.subTotal += this.printSaleItems[i].quantity * this.printSaleItems[i].item_price;

                // ~ fetching item name with the item id in the proforma table ~ //
                this.itemService.findItem({id:this.printSaleItems[i].itemId}).subscribe( res => {
                    if(res[0].name){
                        this.printSaleItems[i].name = res[0].name;
                        this.printSaleItems[i].custom_id = res[0].custom_id;
                        this.printSaleItems[i].subcategory = res[0].subcategory;
                        if(res[0].kg_per_bar != 0){
                            this.printSaleItems[i].kg_per_bar = res[0].kg_per_bar;
                            this.printSaleItems[i].price_per_kg = Math.round((this.printSaleItems[i].item_price / res[0].kg_per_bar) * 100) / 100;
                        }else{
                            this.printSaleItems[i].kg_per_bar = '';
                        }

                    }
                }, errRes => {
                    console.log(errRes);
                })
            }
            if(this.printSaleDetail.withVat) {
                this.printSaleDetail.vat = this.printSaleDetail.subTotal * 0.15;
            }else {
                this.printSaleDetail.vat = this.printSaleDetail.subTotal * 0.075;
            }



            $('#m_modal_4').modal('show');
        }, errRes => {
            toastr.error('Something went wrong !');
            console.log(errRes);
        });

    }

    _print_() {
        window.print();
    }


    onScroll() {
        // ~ all result fetched with date range so no need to load on scroll ~ //
        if(this.showDateRangeClearBtn) return;

        this.offset = this.limit_rows;
        this.limit_rows += this.no_rows_to_load;
        this.billing_service.getAllSales(this.offset,this.no_rows_to_load).subscribe(res => {
            res.map(x => {
                this.proformas.push(x);
            });

            this.dataSource = new MatTableDataSource(this.proformas);
        }, errRes => {
            toastr.error('Can not load proformas');
        });
    }

}