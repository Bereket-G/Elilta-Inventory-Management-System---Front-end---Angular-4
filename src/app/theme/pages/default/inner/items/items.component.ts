import { Component, OnInit} from '@angular/core';
import { ItemService } from "../_services/item.service";
import { Observable } from "rxjs/Observable";
import { Item } from "../_models/item.model";
import { MatTableDataSource} from '@angular/material';

// ~ imported for the ng-bootstrap type ahead ~//
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
//-----------------------------------------------//

import { FormBuilder, FormGroup, Validators} from '@angular/forms';

declare let swal: any;
declare let toastr: any;
declare let $ : any;

@Component({
    selector: 'app-items',
    templateUrl: "./items.component.html",
    styleUrls: ["./items.component.css"]
})
export class ItemsComponent implements OnInit {

    // new Item
    newItemForm: FormGroup;
    updateItemForm: FormGroup;
    newItemUnit: string;

    // show update modal
    showUpdateModal : boolean = false;

    items: Item[];
    itemsCategories : any;
    itemsSubCategories : any;
    itemUnitsOfMeasurement : string[];
    itemsPrices : number[] = [];

    itemDisplayedColumns = ['ID', 'Name', 'Unit', 'Category', 'Sub-Category', 'stock-price', 'Actions'];
    public dataSource;

    // ~ new category for item form ~ //
    newItemCategoryForm : FormGroup;

    selectedCategoryName: string = '';
    selectedCategoryId : number = 0;

    // ~ new sub category form ~ //
    newItemSubCategoryForm : FormGroup;

    // ~ new unit ~ //
    newItemUnitForm : FormGroup;

    constructor(private itemService: ItemService, private fb: FormBuilder) {

        // ~ Fetching item categories ~ //
        this.itemService.getAllItemCategories().subscribe( categories => {
            this.itemsCategories = categories;
        }, errRes => {
            toastr.error(errRes.data[0].msg);
        });


        // ~ Populating items table data
        itemService.getAllItems().subscribe( items_ => {
            this.items = items_;
            this.dataSource = new MatTableDataSource(this.items);

            // setting filter predicate
            this.dataSource.filterPredicate = function customFilter(data , filter:string ): boolean {
                return (data.name.toLowerCase().indexOf(filter) == 0) || (data.custom_id.toLowerCase().indexOf(filter) == 0 );
            }

        }, errRes => {
            toastr.error(errRes.data[0].msg);
        });

        // ~ Managing item units from local storage ~ //
        if(localStorage.getItem("item_units") === null) {
            this.itemUnitsOfMeasurement = ['PCs', 'Bar', 'Sheet', 'Mt', 'Set'];
            localStorage.setItem('item_units', JSON.stringify(this.itemUnitsOfMeasurement));
        } else {
            this.itemUnitsOfMeasurement = JSON.parse(localStorage.getItem("item_units"));
        }

        this.itemsPrices = [ 0 ];

        // ~ new Item Form building ~ //
        this.newItemForm = fb.group({
            'custom_id' : [null, Validators.required],
            'name': [null, Validators.required],
            'color': [null, Validators.required],
            'category': [this.selectedCategoryName, Validators.required],
            'subcategory': [null, Validators.required],
            'unit': [null, Validators.required],
            'kg_per_bar': [null, Validators.required],
            'stock_price': [null, Validators.required],
            'item_description': [null, Validators.required],
            'item_prices': [null, Validators.required],
            'stock_reorder_level': [null, Validators.required]
        });

        // ~ new Item Form building ~ //
        this.updateItemForm = fb.group({
            'id': [null,Validators.required],
            'custom_id' : [null, Validators.required],
            'name': [null, Validators.required],
            'color': [null, Validators.required],
            'category': ['Accessory', Validators.required],
            'subcategory': [null, Validators.required],
            'unit': [null, Validators.required],
            'kg_per_bar': [null, Validators.required],
            'stock_price': [null, Validators.required],
            'item_description': [null, Validators.required],
            'item_prices': [null, Validators.required],
            'stock_reorder_level': [null, Validators.required]
        });

        // ~ new category for Item ~ //
        this.newItemCategoryForm = fb.group({
            'name': [null, Validators.required]
        });

        // ~ new sub category for Item ~ //
        this.newItemSubCategoryForm = fb.group({
            'name': [null, Validators.required],
            'categoryId': [null, Validators.required]
        });

    // ~ new unit and metrics for Item ~ //
        this.newItemUnitForm = fb.group({
            'name': [null, Validators.required]
        });

    }

    ngOnInit() {
        this.itemCategoryChanged('new');
    }

    // ~ Item category changed ~ //
    itemCategoryChanged(whichForm: string) {

        if(this.newItemForm.getRawValue().category == 'Profile'){
            $('.Profile_cat_new').show();
            this.newItemForm.patchValue({
                'unit': 'Bar'
            });
        }else {
            this.newItemForm.patchValue({
                'kg_per_bar': 0
            });
            $('.Profile_cat_new').hide();
        }

        if(this.updateItemForm.getRawValue().category == 'Profile'){
            $('.Profile_cat_update').show();
            this.updateItemForm.patchValue({
                'unit': 'Bar'
            });
        }else {
            this.updateItemForm.patchValue({
                'kg_per_bar': 0
            });
            $('.Profile_cat_update').hide();
        }


        if(whichForm == 'new'){
            this.selectedCategoryName = this.newItemForm.getRawValue().category;
        }else if(whichForm == 'update'){
            this.selectedCategoryName = this.updateItemForm.getRawValue().category;
        }

        if(this.selectedCategoryName == ''){ return ;}

        let chosed_category = this.itemsCategories.find(x => x.name == this.selectedCategoryName);
        this.selectedCategoryId = chosed_category.id;

        // ~ Fetching item sub categories ~ //
        this.itemService.getSubCategoriesOfCategory(this.selectedCategoryId).subscribe( sub_categories => {
            this.itemsSubCategories = sub_categories;
        }, errRes => {
            if(errRes.data[0]){
                toastr.error(errRes.data[0].msg);
            }else {
                toastr.error('Some thing went wrong !');
            }
        });
    }

    showConfirmation_Remove_Item_Swal(event) {
        let target = event.target || event.srcElement || event.currentTarget;
        let __item__ = target.name;
        let __item_id__ = target.id;


        swal({
            title: __item__ +" is about to be removed !",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {

                    let _item_to_be_deleted = this.items.find(x => x.id == __item_id__);
                    if(_item_to_be_deleted != null){
                        this.itemService.delete(_item_to_be_deleted).subscribe( res => {
                            this.updateItemList();
                            toastr.success("Item " + __item__ + " is deleted !");
                        }, err => {
                            swal(err.data['message'], {
                                icon: "error",
                            });
                        });
                    }
                }
            });
    }

    applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
    }

    // Item dynamic price arranging
    addNewPrice() {
        this.itemsPrices.push(0);
    }
    setItemPrice(obj: any) {
        event.preventDefault();
        this.itemsPrices[obj.target.id] =  Number(obj.target.value);
    }

    // new Item form submission
    registerItem(new_Item_Data) {
        let item_prices = Array.from(this.itemsPrices);

        if(new_Item_Data.category == 'Profile'){
            item_prices = item_prices.map( function (val , index) {
               return Math.round((val * new_Item_Data.kg_per_bar) * 100) / 100 ;
            });

            new_Item_Data.stock_price = Math.round((new_Item_Data.stock_price * new_Item_Data.kg_per_bar) * 100) / 100 ;
        }

        new_Item_Data.item_prices = item_prices;
        this.itemService.addItem(new_Item_Data).subscribe(res => {

            this.updateItemList();
            toastr.success("Item Added");

            this.newItemForm.reset();
            // resetting Item prices
            this.itemsPrices.length = 1;
            this.itemsPrices[0] = 0;

        }, errRes => {
            console.log(errRes);
            if(errRes.status === 500){
                toastr.error(errRes.data.message);
                return;
            }

            if(errRes.data[0]){
                errRes.data.forEach( function (item,index) {
                   toastr.error(item.msg);
                });
            }
        });

    }

    updateItemList() {
        this.itemService.getAllItems().subscribe( items_ => {
            this.items = items_;
            this.dataSource = new MatTableDataSource(this.items);

            // setting filter predicate
            this.dataSource.filterPredicate = function customFilter(data , filter:string ): boolean {
                return (data.name.toLowerCase().indexOf(filter) == 0) || (data.custom_id.toLowerCase().indexOf(filter) == 0 );
            }
        });
    }

    // bootstrap typeahead
    typeAheadUnits = (text$: Observable<string>) =>
        text$
            .debounceTime(200)
            .distinctUntilChanged()
            .map(term => this.itemUnitsOfMeasurement.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));

    // ~ new Item category form submit ~ //
    registerNewItemCategory(newItemCategory: any){

        this.itemService.addItemCategory(newItemCategory).subscribe( res => {
            toastr.success('Item category added');

            // ~ updating item list ~ //
            this.itemService.getAllItemCategories().subscribe(res => {
               this.itemsCategories = res;
            });
        }, errRes => {
            toastr.error(errRes.data[0].msg);
        });

        this.newItemCategoryForm.reset();

    }

    showConfirmation_remove_Category_Swal(event) {

        let target = event.target || event.srcElement || event.currentTarget;
        let __category_name = target.name;
        let __category_id = target.id;

        swal({
            title: __category_name +" is about to be removed !",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    let _category_to_be_deleted = this.itemsCategories.find(x => x.id == __category_id);
                    let _cat_index = this.itemsCategories.findIndex(d => d.id == __category_id);
                    if(_category_to_be_deleted != null){
                        this.itemService.delete(_category_to_be_deleted).subscribe( res => {
                            this.itemsCategories.splice(_cat_index, 1);
                            toastr.success("Category " + __category_name + " is deleted !");
                        }, err => {
                            swal(err.data['msg'], {
                                icon: "error",
                            });
                        });
                    }

                }
            });
    }


    // onclick of a category
    categorySelected(event : any){
        if (event.srcElement.firstElementChild) {
            this.selectedCategoryName = event.srcElement.firstElementChild.innerHTML.trim();
            this.selectedCategoryId = event.srcElement.firstElementChild.id.trim();

        }else {
            this.selectedCategoryName = event.srcElement.innerHTML.trim();
            this.selectedCategoryId = event.srcElement.id.trim();
        }

        this.itemService.getSubCategoriesOfCategory(this.selectedCategoryId).subscribe(res => {
            this.itemsSubCategories = res;
        }, errRes => {
           toastr.error('Unable to fetch subcategories ');
        });

    }

    // ~ new Sub category form submit ~ //
    registerNewItemSubCategory(newItemSubCategory: any){

        if(this.selectedCategoryId == 0){
            toastr.warning('Choose category first !');
            return;
        }

        this.itemService.addItemSubCategory(newItemSubCategory).subscribe( res => {
            toastr.success('Item sub category added');

            // ~ Fetching item sub categories ~ //
            this.itemService.getSubCategoriesOfCategory(newItemSubCategory.categoryId).subscribe( sub_categories => {
                this.itemsSubCategories = sub_categories;
            }, errRes => {
                if(errRes.data[0]){
                    toastr.error(errRes.data[0].msg);
                }else {
                    toastr.error('Some thing went wrong !');
                }
            });

        }, errRes => {
            if(errRes.data[0]){
                toastr.error(errRes.data[0].msg); return;
            }
            toastr.error("Something Went wrong !");
        });

        this.newItemSubCategoryForm.reset();

    }

    showConfirmation_remove_Sub_Category_Swal(event) {

        let target = event.target || event.srcElement || event.currentTarget;
        let __sub_category_name = target.name;
        let __sub_category_id = target.id;

        swal({
            title: "Sub Categoy "+ __sub_category_name +" is about to be removed !",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    let sub_category_to_be_deleted = this.itemsSubCategories.find(x => x.id == __sub_category_id);
                    let _sub_cat_index = this.itemsSubCategories.findIndex(d => d.id == __sub_category_id);
                    if(sub_category_to_be_deleted != null){
                        this.itemService.delete(sub_category_to_be_deleted).subscribe( res => {
                            this.itemsSubCategories.splice(_sub_cat_index, 1);
                            toastr.success(" Sub Category " + __sub_category_name + " is deleted !");
                        }, err => {
                            swal(err.data['msg'], {
                                icon: "error",
                            });
                        });
                    }

                }
            });
    }

    showConfirmation_remove_Item_Unit_Swal(event) {

        let target = event.target || event.srcElement || event.currentTarget;
        let __unit_name = target.name;

        swal({
            title: "Item unit "+ __unit_name +" is about to be removed !",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    let index = this.itemUnitsOfMeasurement.indexOf(__unit_name);
                    if (index > -1) {
                        this.itemUnitsOfMeasurement.splice(index, 1);
                        localStorage.setItem('item_units', JSON.stringify(this.itemUnitsOfMeasurement));
                        toastr.success('Item unit deleted');
                    }
                }
            });
    }

    registerNewItemUnit(ItemUnit : any) {
        this.itemUnitsOfMeasurement.push(String(ItemUnit.name));
        localStorage.setItem('item_units', JSON.stringify(this.itemUnitsOfMeasurement));
        toastr.success('New item unit added');
        this.newItemUnitForm.reset();
    }

    itemClicked(item : any) {

        this.itemService.findItemPrices(item.id).subscribe( res => {

            this.itemsPrices = [];

            for(let i = 0; i < res.length; i++){
                this.itemsPrices.push(res[i].value);
            }

            this.updateItemForm.patchValue({
                'id': item.id,
                'custom_id' : item.custom_id,
                'name': item.name,
                'color': item.color,
                'category': item.category,
                'subcategory': item.subcategory,
                'unit': item.unit,
                'kg_per_bar': item.kg_per_bar,
                'stock_price': item.stock_price,
                'item_description': item.item_description,
                'stock_reorder_level': item.stock_reorder_level
            });

            if(this.updateItemForm.getRawValue().category == 'Profile'){
                let temp_kg_per_bar = this.updateItemForm.getRawValue().kg_per_bar;
                this.itemsPrices = this.itemsPrices.map( function (val , index) {
                    return  Math.round((val / (temp_kg_per_bar)) * 100) / 100 ;
                });

                this.updateItemForm.patchValue({
                    'stock_price':  Math.round((item.stock_price / (temp_kg_per_bar)) * 100) / 100
                });

            }

            this.itemCategoryChanged('update');

            $('#m_modal_Update').modal('show');

            this.showUpdateModal = true;


        }, err => {
            toastr.error('Something Went wrong !');
        });

    }

    updateItem(itemData : any){

        let item_prices = Array.from(this.itemsPrices);

        if(itemData.category == 'Profile'){
            item_prices = item_prices.map( function (val , index) {
                return Math.round((val * itemData.kg_per_bar) * 100) / 100 ;
            });

            itemData.stock_price = Math.round((itemData.stock_price * itemData.kg_per_bar) * 100) / 100 ;
        }
        itemData.item_prices = item_prices;

        this.itemService.updateItem(itemData.id ,itemData).subscribe(res => {

            this.updateItemList();
            toastr.success("Item Updated");

            this.updateItemForm.reset();
            // resetting Item prices
            this.itemsPrices.length = 1;
            this.itemsPrices[0] = 0;

        }, errRes => {
            if(errRes.data[0]){
                errRes.data.forEach( function (item,index) {
                    toastr.error(item.msg);
                });
            }
        });


    }


}
