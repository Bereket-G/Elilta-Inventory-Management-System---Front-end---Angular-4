import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { BillingService } from "../_services/billing.service";
import { ScriptLoaderService } from "../../../../../_services/script-loader.service";
import { Subscription } from 'rxjs';
import {ItemService} from "../_services/item.service";

declare let swal: any;
declare let toastr: any;
declare let $: any;

@Component({
  selector: 'app-proforma',
  templateUrl: "proforma.component.html",
  styleUrls: ["./proforma.component.css"]
})
export class ProformaComponent implements OnInit {

    // ~ paging variables ~ //
    private offset : number = 0;
    private limit_rows : number = 15;
    private no_rows_to_load :number = 7;

    // ~ proforma to be printed data ~ //
    printProformaItems : any = [];
    private printProformaDetail : any = {};

    // ~ for ng busy show loader ~ //
    busy: Subscription;

    showDateRangeClearBtn : boolean = false;
    proformas: any;
    daterange: any = {};
    public dataSource;
    proformaDisplayedColumns = ['Id', 'Bill_To', 'Address', 'Proforma_Date', 'Amount', 'Actions'];

    constructor(private itemService: ItemService,private billing_service: BillingService) {
        this.busy = this.billing_service.getAllProformas(this.offset, this.limit_rows).subscribe(res => {
            this.proformas = res;
            this.dataSource = new MatTableDataSource(this.proformas);
        }, errRes => {
            toastr.error('Can not load proformas');
        });
    }

    ngOnInit() {

    }


    applyFilter(filterValue: string) {
        if(filterValue.length == 0){
            // limit will be changed later.
            this.busy = this.billing_service.getAllProformas(0, 3).subscribe(res => {
                this.proformas = res;
                this.dataSource = new MatTableDataSource(this.proformas);
            }, errRes => {
                toastr.error('Can not load proformas');
            });
            return;
        }

        if(filterValue.length < 2) return;

       this.busy = this.billing_service.searchProformaWithKeyword(filterValue).subscribe( res => {
           this.proformas = res;
           this.dataSource = new MatTableDataSource(this.proformas);
       }, errRes => {
           toastr.error('Something went wrong while searching table');
       });
    }

    showConfirmation_Remove_proforma_Swal(event) {
        let target = event.target || event.srcElement || event.currentTarget;
        let __customer__ = target.name;
        let __proforma_id_ = target.id;


        swal({
            title: "Proforma to " + __customer__ + " is about to be removed !",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {

                    let _item_to_be_deleted = this.proformas.find(x => x.id == __proforma_id_);
                    if (_item_to_be_deleted != null) {
                        BillingService.delete(_item_to_be_deleted).subscribe(res => {
                            this.updateList();
                            toastr.success("Proforma deleted !");
                        }, err => {
                            swal(err.data['message'], {
                                icon: "error",
                            });
                        });
                    }
                }
            });
    };

    selectWithDateRange(value: any) {
        this.showDateRangeClearBtn = true;

        this.daterange.start = value.start.format("YYYY-MM-DD");
        this.daterange.end = value.end.format("YYYY-MM-DD");

        this.billing_service.searchProformaByDateRange(this.daterange.start, this.daterange.end).subscribe(res => {
            this.proformas = res;
            this.dataSource = new MatTableDataSource(this.proformas);
        }, err => {
            toastr.error("Something went wrong");
            console.log(err);
        });
    }


    dateRangeClear(){
        this.showDateRangeClearBtn = false;
        // ~ _limit is the first numbers of row to load ~ //
        this.billing_service.getAllProformas(0, 15).subscribe(res => {
            this.proformas = res;
            this.dataSource = new MatTableDataSource(this.proformas);
        }, errRes => {
            toastr.error('Can not load proformas');
            console.log(errRes);
        });
    }

    updateList() {
        this.busy = this.billing_service.getAllProformas(this.offset,this.limit_rows).subscribe(res => {
            this.proformas = res;
            this.dataSource = new MatTableDataSource(this.proformas);
        }, errRes => {
            toastr.error('Can not load proformas');
        });
    }

    // ~ proforma is selected so print view should be generated ~ //
    proforma_clicked(proforma_Data) {

        this.printProformaDetail = proforma_Data;
        this.printProformaItems = [];
        this.busy = this.billing_service.getItemOfProformaBill(proforma_Data.id).subscribe( res => {

            this.printProformaItems = res.map( i => i);
            this.printProformaDetail.subTotal = 0;
            for (let i = 0; i < this.printProformaItems.length ; i++){
                // ~ calculating subtotal ~ //
                this.printProformaDetail.subTotal += this.printProformaItems[i].quantity * this.printProformaItems[i].item_price;

                // ~ fetching item name with the item id in the proforma table ~ //
                this.itemService.findItem({id:this.printProformaItems[i].itemId}).subscribe( res => {
                    if(res[0].name){
                        this.printProformaItems[i].name = res[0].name;
                        this.printProformaItems[i].custom_id = res[0].custom_id;
                        this.printProformaItems[i].sub_category = res[0].subcategory;
                        if(res[0].kg_per_bar != 0){
                            this.printProformaItems[i].kg_per_bar = res[0].kg_per_bar;
                            this.printProformaItems[i].price_per_kg = Math.round((this.printProformaItems[i].item_price / res[0].kg_per_bar) * 100) / 100;
                        }else{
                            this.printProformaItems[i].kg_per_bar = '';
                        }

                    }
                }, errRes => {
                    console.log(errRes);
                })
            }
            this.printProformaDetail.vat = this.printProformaDetail.subTotal * 0.15;



            $('#m_modal_4').modal('show');
        }, errRes => {
            console.log(errRes);
        });

    }

    _print_() {
        window.print();
    }

    onScroll() {
        // ~ all result fetched with date range so no need to load on scroll ~ //
        if(this.showDateRangeClearBtn) return;

        this.offset = this.limit_rows;
        this.limit_rows += this.no_rows_to_load;
        this.billing_service.getAllProformas(this.offset,this.no_rows_to_load).subscribe(res => {
            res.map(x => {
               this.proformas.push(x);
            });

            this.dataSource = new MatTableDataSource(this.proformas);
        }, errRes => {
            toastr.error('Can not load proformas');
        });
    }

}