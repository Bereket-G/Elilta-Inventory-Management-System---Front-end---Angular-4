import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material'
import { CustomerService } from "../_services/customer.service";
import { Customer } from "../_models/customer.model";

import { FormBuilder, FormGroup, Validators} from '@angular/forms';

declare let swal: any;
declare let toastr: any;

@Component({
  selector: 'app-customers',
  templateUrl: "./customers.component.html",
  styleUrls: ["./customers.component.css"]
})
export class CustomersComponent implements OnInit {

    newCustomerForm: FormGroup;

    customers: Customer[];

    customersDisplayedColumns = ['ID', 'Name', 'TIN', 'Phone No', 'Address', 'Actions'];
    public dataSource;

    constructor(private fb: FormBuilder, private customerService: CustomerService) {
      this.newCustomerForm = fb.group({
          'name':[null, Validators.required],
          'TIN_NO' : [null, Validators.required],
          'phone' : [null, Validators.required],
          'address': [null]
      });

      this.customerService.getAllCustomers().subscribe( res => {
          this.customers = res;
          this.dataSource = new MatTableDataSource(this.customers);
          console.log(res);
      }, err => {
          console.log(err);
          swal(err.data['msg'], {
              icon: "error",
          });
      });
    }

    ngOnInit() {
    }

    applyFilter(filterValue: string) {
      filterValue = filterValue.trim(); // Remove whitespace
      filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
      this.dataSource.filter = filterValue;
    }

    formSubmit(newCustomer : any){
        this.newCustomerForm.reset();
        this.customerService.addCustomer(newCustomer).subscribe(res => {
            toastr.success('Customer added');

            // ~ updating item lists ~ //
            this.updateCustomerList();
        }, errRes => {
            toastr.error(errRes.data.msg);
        });
    }

    updateCustomerList() {
        this.customerService.getAllCustomers().subscribe(res =>{
            this.customers = res;
            this.dataSource = new MatTableDataSource(this.customers);
        });
    }


    showConfirmation_Remove_Item_Swal(event) {
        let target = event.target || event.srcElement || event.currentTarget;
        let __customer__ = target.name;
        let __customer_id__ = target.id;


        swal({
            title: __customer__ +" is about to be removed !",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    let _customer_to_be_deleted = this.customers.find(x => x.id == __customer_id__);
                    if(_customer_to_be_deleted != null){
                        CustomerService.delete(_customer_to_be_deleted).subscribe(res => {
                            this.updateCustomerList();
                            toastr.success("Customer " + __customer__ + " is deleted !");
                        }, err => {
                            swal(err.data['message'], {
                                icon: "error",
                            });
                        });
                    }
                }
            });
    }

}
