import { Component, OnInit } from '@angular/core';
import {ScriptLoaderService} from "../../../../../_services/script-loader.service";
import { Proforma } from "../_models/proforma.model";
import { CustomerService } from "../_services/customer.service";
import { ItemService } from "../_services/item.service";
import { BillItem } from "../_models/BillItems.model";
import { BillingService } from "../_services/billing.service";

import { DatePipe } from '@angular/common';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';

import { MatTableDataSource} from '@angular/material';

// ~ ng-b typeahead ~ //
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

declare let toastr: any;

@Component({
  selector: 'app-new-proforma',
  templateUrl: './new-proforma.component.html',
  styleUrls: ['./new-proforma.component.css']
})
export class NewProformaComponent implements OnInit {

    pricePerKg : number = 171;

    // ~ proforma list for select table view ~ //
    selectedRowIndex: number = -1;
    itemDisplayedColumns = [ 'Item-Code','Name', 'Unit', 'Sub-category','unit-price' ];
    public dataSource;

    newProforma : Proforma;
    newProformaForm : FormGroup;

    customers : any[] = [];
    customerNames : string[] = [];

    itemNamePlaceHolder: string ="Choose Category first";
    itemCategories : string[]; // list for choose category drop down
    selectedItemCategory : string;
    selectedItemSubCategory: string;
    selectedItemCustomId : string;
    selectedItemId: number;
    selectedItemName : string;
    selectedItemprice : string;
    selectedItemUnit : string;
    selectedItemKgPerBar : number = null;
    itemQuantity: number = 0;
    totalPrice: number;
    selectedItemPriceValue : number = 0;
    items : any [] = [];
    itemNames : string[] = []; // list of item names fetched with category chosen
    itemIds : string[] = []; // list of item ids fetched with category chosen
    selectedItemPrices : string[] = [];
    selectedItemPricesValues : number[] = [];

    // ~ items for proforma ~ //
    proformaItems : BillItem[] = [];

    subTotal : number = 0;
    grandTotal: number = 0;

  constructor(private customerService: CustomerService, private itemService: ItemService,
              private _script: ScriptLoaderService, private fb: FormBuilder , private billingService :BillingService,
              private datePipe: DatePipe) {
      let today = new Date();

      this.newProformaForm = fb.group({
          'bill_to' : [null, Validators.required],
          'TIN_NO' : [''],
          'address' : [''],
          'proforma_date' : datePipe.transform(today, 'yyyy-MM-dd'),
          'reference': [null],
          'FSNO' : [null],
          'discount' : [0],
          'amount' : [null,Validators.required]
      });

      this.itemService.getAllItemCategories().subscribe(res => {
         this.itemCategories = res;
      }, errRes => {
          toastr.error("Error while fetching categories.");
      });

      // ~ Populating items table data
      itemService.getAllItems().subscribe( items_ => {
          this.items = items_;
          this.dataSource = new MatTableDataSource(this.items);

          // setting filter predicate with id and name column only
          this.dataSource.filterPredicate = function customFilter(data , filter:string ): boolean {
              return (data.name.toLowerCase().indexOf(filter) == 0) || (data.custom_id.toLowerCase().indexOf(filter) == 0 );
          }

      }, errRes => {
          toastr.error(errRes.data[0].msg);
      });

      // getting last stored pricePerKg
      if(localStorage.getItem("pricePerKg") === null ) {
              localStorage.setItem('pricePerKg', ( ''+this.pricePerKg));
      } else {
          this.pricePerKg = parseInt(localStorage.getItem("pricePerKg"));
      }

  }

  ngOnInit() {
      this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper',
          'assets/demo/bootstrap-datepicker.js');
  }

  _print_(proformaFormDetails) {


      if(this.subTotal <= 0){
          toastr.error(' Can not save Invalid Proforma ! ');
          return false;
      }
      // ~ saving the proforma ~ //
      this.billingService.saveProforma(proformaFormDetails, this.proformaItems).subscribe( res => {
              toastr.success('Proforma saved !');

              window.print();
              // ~ resetting proforma item details ~ //
              this.selectedItemName = '';
              this.selectedItemCustomId = '';
              this.selectedItemId = null;
              this.selectedItemPriceValue = 0;
              this.selectedItemPricesValues = [];
              this.selectedItemPrices = [];
              this.selectedItemUnit = '';
              this.selectedItemCategory = '';
              this.selectedItemSubCategory = '';
              this.selectedItemKgPerBar = null;
              this.selectedItemprice = '';
              this.itemQuantity = 0;
              this.totalPrice = 0;
              this.proformaItems = [];
              this.subTotal = 0;

              // ~ reset form ~ //
              let today = new Date();
              this.newProformaForm.reset({
                  'proforma_date' : this.datePipe.transform(today, 'yyyy-MM-dd')});
              return true;
          }, errRes => {
              toastr.error(' Proforma not saved. Something went wrong !');
              return false;
          }
      );

  }

    searchCustomer = (text$: Observable<string>) =>
        text$
            .debounceTime(200)
            .distinctUntilChanged()
            .do((term) => {
                this.customerService.findCustomer({name: term}).subscribe( res => {

                    for(let i = 0; i < res.length; i++){
                        if(-1 == this.customerNames.findIndex(k => k == res[i].name)){
                            this.customers.push(res[i]);
                            this.customerNames.push(res[i].name);
                        }
                    }
                })
            })
            .map(term =>
                 this.customerNames.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));


    selectedCustomer(customer){
        for (let i = 0; i < this.customers.length ; i++){
            if(this.customers[i].name == customer.item){
                this.newProformaForm.patchValue({
                    bill_to : this.customers[i].name,
                    TIN_NO : this.customers[i].TIN_NO,
                    address : this.customers[i].address
                });
            }
        }
    }


    categorySelected(){
        this.itemNamePlaceHolder = "";
        this.selectedItemCustomId='';
        this.selectedItemId = null;
        this.selectedItemPrices = [];
        this.selectedItemName = '';
        this.items = [];
        this.itemNames = [];
        this.selectedItemPriceValue = 0;
        this.selectedItemUnit = '';

        this.itemService.findItem({category : this.selectedItemCategory }).subscribe( res => {
            this.items = res;
            this.dataSource = new MatTableDataSource(this.items);
        }, errRes => {
            toastr.error('Something went wrong !');
        });

    }


    searchItemName = (text$: Observable<string>) =>
        text$
            .debounceTime(200)
            .do((term) => {
                this.itemService.findItemLike({value: term, category : this.selectedItemCategory, col : 'name' }).subscribe( res => {

                    for(let i = 0; i < res.length; i++){
                        if(-1 == this.itemNames.findIndex(k => k == res[i].name)){
                            this.items.push(res[i]);
                            this.itemNames.push(res[i].name);
                        }
                    }
                })
            })
            .map(term =>
                this.itemNames.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));



    searchItemId = (text$: Observable<string>) =>
        text$
            .debounceTime(200)
            .do((term) => {
                this.itemService.findItemLike({value: term, category : this.selectedItemCategory,  col : 'custom_id' }).subscribe( res => {

                    for(let i = 0; i < res.length; i++){
                        if(-1 == this.itemIds.findIndex(k => k == res[i].custom_id)){
                            this.items.push(res[i]);
                            this.itemIds.push(res[i].custom_id);
                        }
                    }
                })
            })
            .map(term =>
                this.itemIds.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));


    itemNameSelected(option) {
        this.selectedItemPriceValue = 0;
        this.selectedItemPrices = [];
        this.selectedItemPricesValues = [];
        this.selectedItemprice = '';
        for (let i = 0; i < this.items.length ; i++){
            if(this.items[i].name == option.item){
                this.selectedItemUnit = this.items[i].unit;
                this.selectedItemCustomId = this.items[i].custom_id;
                this.selectedItemId = this.items[i].id;
                this.itemService.findItemPrices(this.items[i].id).subscribe( res => {
                    for (let x = 0; x < res.length; x++){
                        if(-1 == this.selectedItemPricesValues.findIndex(k => k == res[x].value)) {
                            this.selectedItemPrices.push(res[x].name);
                            this.selectedItemPricesValues.push(res[x].value);
                        }
                    }
                });
            }
        }
    }

    itemIdSelected(option) {
        this.selectedItemPriceValue = 0;
        this.selectedItemPrices = [];
        this.selectedItemPricesValues = [];
        this.selectedItemprice = '';
        for (let i = 0; i < this.items.length ; i++){
            if(this.items[i].custom_id == option.item){
                this.selectedItemUnit = this.items[i].unit;
                this.selectedItemName = this.items[i].name;
                this.itemService.findItemPrices(this.items[i].id).subscribe( res => {
                    for (let x = 0; x < res.length; x++){
                        if(-1 == this.selectedItemPricesValues.findIndex(k => k == res[x].value)) {
                            this.selectedItemPrices.push(res[x].name);
                            this.selectedItemPricesValues.push(res[x].value);
                        }
                    }
                });
            }
        }
    }

    itemPriceChosed(){
        for (let x = 0; x < this.selectedItemPrices.length; x++){
            if(this.selectedItemPrices[x] == this.selectedItemprice) {
                this.selectedItemPriceValue = this.selectedItemPricesValues[x];
            }
        }
        this.calculateTotalPrice();
    }

    calculateTotalPrice(){
        this.totalPrice = this.selectedItemPriceValue * this.itemQuantity;
    }

    addToProformaItems(){
        let newProformaItem = new BillItem();
        newProformaItem.name = this.selectedItemName;
        newProformaItem.item_price = this.selectedItemPriceValue;
        newProformaItem.custom_id = this.selectedItemCustomId;
        newProformaItem.itemId = this.selectedItemId;
        newProformaItem.subcategory = this.selectedItemSubCategory;
        newProformaItem.kg_per_bar = this.selectedItemKgPerBar;
        newProformaItem.quantity = this.itemQuantity;
        newProformaItem.unit = this.selectedItemUnit;

        if( this.itemQuantity <= 0 || this.selectedItemPriceValue <= 0 ){
            toastr.warning("Fill out required fields !");
            return;
        }

        // if the item is previously added to the sale list
        for(let i = 0; i < this.proformaItems.length; i++ ) {
            if(this.proformaItems[i].itemId == this.selectedItemId){
                this.proformaItems[i].quantity += this.itemQuantity;

                // ~ resetting proforma item details ~ //
                this.selectedItemName = '';
                this.selectedItemCustomId = '';
                this.selectedItemId = null;
                this.selectedItemPriceValue = 0;
                this.selectedItemPricesValues = [];
                this.selectedItemPrices = [];
                this.selectedItemUnit = '';
                this.selectedItemCategory = '';
                this.selectedItemSubCategory = '';
                this.selectedItemKgPerBar = null;
                this.selectedItemprice = '';
                this.itemQuantity = 0;
                this.totalPrice = 0;

                this.selectedRowIndex = -1;

                this.calculateSubTotal();
                return;
            }
        }
        
        this.proformaItems.push(newProformaItem);

        // ~ resetting proforma item details ~ //
        this.selectedItemName = '';
        this.selectedItemCustomId = '';
        this.selectedItemId = null;
        this.selectedItemPriceValue = 0;
        this.selectedItemPricesValues = [];
        this.selectedItemPrices = [];
        this.selectedItemUnit = '';
        this.selectedItemCategory = '';
        this.selectedItemSubCategory = '';
        this.selectedItemKgPerBar = null;
        this.selectedItemprice = '';
        this.itemQuantity = 0;
        this.totalPrice = 0;

        this.selectedRowIndex = -1;
        this.calculateSubTotal();


    }

    calculateSubTotal() {

        // check if data is valid to calculate subtotal
        if( this.pricePerKg < 0 ){
            toastr.error('Price per Kg value is not correct !');
            return;
        }


        this.subTotal = 0;
        for (let i = 0; i < this.proformaItems.length ; i++){

            if(this.proformaItems[i].kg_per_bar != null && this.proformaItems[i].kg_per_bar){
                this.subTotal += this.proformaItems[i].quantity * this.proformaItems[i].kg_per_bar * this.pricePerKg;
                continue;
            }

            this.subTotal += this.proformaItems[i].quantity * this.proformaItems[i].item_price;
        }
        this.calculateGrandTotal();
    }

    calculateGrandTotal() {
        this.grandTotal = ((this.subTotal + (this.subTotal * 0.15) - ((this.newProformaForm.getRawValue().discount/100) * this.subTotal)) );
    }

    // ~ on submit of the form ~ //
    saveProforma(proformaFormDetails) {

        // Save the new price per Kg
        localStorage.setItem('pricePerKg', ( ''+this.pricePerKg));

        if(this.subTotal <= 0){
            toastr.error(' Can not save Invalid Proforma ! ');
            return false;
        }
        // ~ saving the proforma ~ //
        this.billingService.saveProforma(proformaFormDetails, this.proformaItems).subscribe( res => {
                toastr.success('Proforma saved !');

            // ~ saving customer ~ //
            let newCustomer = {
                name: proformaFormDetails.bill_to,
                TIN_NO: proformaFormDetails.TIN_NO
            };
            this.customerService.addCustomer(newCustomer).subscribe(res => {
                toastr.success('Customer added');
            });

            // ~ resetting proforma item details ~ //
            this.selectedItemName = '';
            this.selectedItemCustomId = '';
            this.selectedItemId = null;
            this.selectedItemPriceValue = 0;
            this.selectedItemPricesValues = [];
            this.selectedItemPrices = [];
            this.selectedItemUnit = '';
            this.selectedItemCategory = '';
            this.selectedItemSubCategory = '';
            this.selectedItemKgPerBar = null;
            this.selectedItemprice = '';

            this.itemQuantity = 0;
            this.totalPrice = 0;
            this.proformaItems = [];
            this.subTotal = 0;

            // ~ reset form ~ //
            let today = new Date();
            this.newProformaForm.reset({
                'proforma_date' : this.datePipe.transform(today, 'yyyy-MM-dd')});
                return true;
            }, errRes => {
                toastr.error(' Proforma not saved. Something went wrong !');
                return false;
            }
        );


    }


    // Item selected form list of items for proforma list
    itemRowSelected(row){

        // ~ reset some values ~ //
        this.selectedItemPriceValue = 0;
        this.selectedItemPrices = [];
        this.selectedItemPricesValues = [];
        this.selectedItemprice = '';
        this.selectedItemId = row.id;
        this.selectedRowIndex = row.id;
        this.selectedItemUnit = row.unit;
        this.selectedItemCustomId = row.custom_id;
        this.selectedItemCategory = row.category;
        this.selectedItemSubCategory = row.subcategory;
        this.selectedItemName = row.name;

        this.items.forEach((val,indx) => {
            if(row.id === val.id && val.kg_per_bar > 0){
                this.selectedItemKgPerBar = val.kg_per_bar;
            }
        });

        this.itemService.findItemPrices(row.id).subscribe( res => {
            for (let x = 0; x < res.length; x++){
                if(-1 == this.selectedItemPricesValues.findIndex(k => k == res[x].value)) {
                    this.selectedItemPrices.push(res[x].name);
                    this.selectedItemPricesValues.push(res[x].value);
                }
            }
        });
    }

    applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    }

    proformaItemListClicked(itemName : any){
        console.log('Item selected !' + itemName);
        this.selectedItemName = itemName;
    }

    removeItemFromProformaItems(){
        let item_x_name = this.selectedItemName;
        let itemToRemoveIndex = -1;
        this.proformaItems.map(function (item_, index_) {
            if(item_.name === item_x_name){
                itemToRemoveIndex = index_;
            }
        });
        if(itemToRemoveIndex == -1 ){
            toastr.warning('Item not in sale list !');
        }else{
            this.proformaItems.splice(itemToRemoveIndex,1);
            // ~ resetting proforma item details ~ //
            this.selectedItemName = '';
            this.selectedItemCustomId = '';
            this.selectedItemId = null;
            this.selectedItemPriceValue = 0;
            this.selectedItemPricesValues = [];
            this.selectedItemPrices = [];
            this.selectedItemUnit = '';
            this.selectedItemCategory = '';
            this.selectedItemSubCategory = '';
            this.selectedItemKgPerBar = null;
            this.selectedItemprice = '';
            this.itemQuantity = 0;
            this.totalPrice = 0;

            this.selectedRowIndex = -1;
            this.calculateSubTotal();
        }

    }

}
