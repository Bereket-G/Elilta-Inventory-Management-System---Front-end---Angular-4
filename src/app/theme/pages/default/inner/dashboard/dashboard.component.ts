import {Component, OnInit, ViewChild} from '@angular/core';
import { StockService } from "../_services/stock.service";
import { BaseChartDirective }   from 'ng2-charts/ng2-charts';
import { BillingService } from "../_services/billing.service";
import { ItemService } from "../_services/item.service";
import { DatePipe } from '@angular/common';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';

// ~ Form Import ~ //
import { FormBuilder, FormGroup, Validators} from '@angular/forms';

// ~ ng-b typeahead ~ //
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

declare let toastr: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: "dashboard.component.html",
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

    @ViewChild("baseChart") stockChart: BaseChartDirective;
    @ViewChild("saleChart") salesChart: BaseChartDirective;

    // ~ stock in history log variables ~ //
    public StockIns;

    // ~ Paging with month for the monthly item wise sale report ~ //
    private pg_month : number;
    private pg_year : number;

    // ~ Paging variables ~ //
    private offset : number = 0;
    private limit_rows : number = 7;
    private no_rows_to_load :number = 4; // for now let it be one

    // ~ Stock In ~ //
    stockInSearchTerm: string = '';
    stockInForm : FormGroup;
    items: any[] = [];
    stockInItemNames: string [] = [];
    selectedItemName: string = '';
    selectedCategory: string = null;
    selectedSubCategory: string= null;
    stockInItemPrice: number = 0;
    stockInValue: number = 0;

    itemsCategories: any;
    itemSubCategories: any[] = [];
    itemsInStock : number;
    stockValueInBirr : number;
    stockValueCalcuated_with_price1_InBirr : number;
    todaySaleAmount : number = 0;

    // ~ Stock report bar chart ~ //
    stockChartPopulateBy: string = "category";
    itemsQuantities: number[] = [];
    public StockChartOptions:any = {
        scaleShowVerticalLines: false,
        responsive: true
    };
    public StockChartLabels:string[] = [];
    public StockChartType:string = 'bar';
    public StockChartLegend:boolean = true;
    public StockChartData:any[] = [
        {data: this.itemsQuantities , label: 'quantity'}
    ];

    // ~ Sales bar chart report ~ //
    salesReportFrequency: string = "Daily";
    soldAmounts: number[] = [];
    public salesChartOptions:any = {
        scaleShowVerticalLines: false,
        responsive: true
    };
    public salesChartLabels:string[] = [];
    public salesChartType:string = 'bar';
    public salesChartLegend:boolean = true;

    public salesChartData:any[] = [
        {data: this.soldAmounts, label: 'Birr'}
    ];




    constructor(private stockService: StockService,
                private fb: FormBuilder,
                private billingService: BillingService,
                private datePipe: DatePipe,
                private itemService: ItemService) {

                    let today = new Date();
                    this.stockInForm = fb.group({
                        'Name' : [null, Validators.required],
                        'quantity' : [null, Validators.required],
                        'value' : [null, Validators.required],
                        'itemId': [null, Validators.required],
                        'reoder_level': [null, Validators.required],
                        'date_in' : [datePipe.transform(today, 'yyyy-MM-dd')]
                    });

                    this.stockService.getStockInHistory().subscribe(res => {
                        this.StockIns = res.map(i => {return i});
                        //setting stock in search filter predicate to search with item name and date
                        this.StockIns.filterPredicate = function customFilter(data , filter:string ): boolean {
                            return (data.Name.toLowerCase().indexOf(filter) == 0) || (data.date_in.toLowerCase().indexOf(filter) == 0 );
                        }
                    }, err => {
                        toastr.error('Unable to fetch stock in history');
                    });


        // setting paging variable with current month
        this.pg_month = today.getMonth() + 1;
        this.pg_year = today.getFullYear();
        console.log(this.pg_year + ' ' + this.pg_month);
    }

    ngOnInit() {

        // ~ fetching stock details ~ //
        this.stockService.getStockDetails().subscribe( res => {
            this.itemsInStock = res[0].items_available;
            this.stockValueInBirr = res[0].value_of_stock;
            this.stockValueCalcuated_with_price1_InBirr = res[0].stock_calculated_value;
        }, errRes => {
            toastr.error('Unable to fetch Stock details !');
        });

        //fetching today sale value
        this.billingService.getTodaysSale().subscribe(res => {
            if(res[0]){
                this.todaySaleAmount = res[0].value;
            }
        }, errRes => {
            toastr.error('Unable to fetch data !');
        });

        // ~ fetching stock report ~ //
        this.stockService.getStockValueWithCategory().subscribe( res=> {
            for (let i = 0; i<res.length; i++){
              this.StockChartLabels.push(res[i].name);
              this.itemsQuantities.push( parseInt(res[i].quantities));
            }

            this.stockChart.chart.update();
        }, errRes => {
            toastr.error('Unable to fetch stock report !');
        });

        // ~ fetching sales report ~ //
        this.billingService.getDailyReportSales(this.offset, this.limit_rows).subscribe( res=> {
            for (let i = 0; i<res.length; i++){
                this.salesChartLabels.push(res[i].date);
                this.soldAmounts.push( parseInt(res[i].sale_value));
            }

            this.salesChart.labels = this.salesChartLabels;
            this.salesChart.datasets = this.salesChartData;
            this.salesChart.chart.update();

        }, errRes => {
            toastr.error('Unable to fetch sales report !');
        });


        // ~ Fetching item categories ~ //
        this.itemService.getAllItemCategories().subscribe( categories => {
            this.itemsCategories = categories;
        }, errRes => {
            toastr.error(errRes.data[0].msg);
        });

    }

    updateStockChart(){

        this.StockChartLabels = [];
        this.itemsQuantities = [];
        this.StockChartData[0].data = [];

        if(this.stockChartPopulateBy == "category"){
            this.stockService.getStockValueWithCategory().subscribe( res=> {
                for (let i = 0; i<res.length; i++){
                    this.StockChartLabels.push(res[i].name);
                    this.StockChartData[0].data.push(res[i].quantities);
                    this.itemsQuantities.push( parseInt(res[i].quantities));
                }

                this.stockChart.chart.update();


            }, errRes => {
                toastr.error('Something went wrong !');
            });
        } else if(this.stockChartPopulateBy == "product") {
            this.stockService.getStockValueWithProduct().subscribe( res=> {
                for (let i = 0; i<res.length; i++){
                    this.StockChartLabels.push(res[i].item_name);
                    this.StockChartData[0].data.push(res[i].quantities);
                    this.itemsQuantities.push( parseInt(res[i].quantities) );
                }

                this.stockChart.chart.update();

            }, errRes => {
                toastr.error('Something went wrong !');
            });
        }

    }

    populateByCategory(){
        this.stockChartPopulateBy = "category";
        this.updateStockChart();
        this.stockChart.chart.update();
    }

    populateByProduct(){
        this.stockChartPopulateBy = "product";
        this.updateStockChart();
        this.stockChart.chart.update();
    }


    updateSalesChart(){
        this.salesChartLabels = [];
        this.soldAmounts = [];
        this.salesChartData[0].data = [];

        if(this.salesReportFrequency == "Monthly"){
            this.billingService.getMonthlyReportSales(this.offset, this.limit_rows).subscribe( res=> {
                for (let i = 0; i<res.length; i++){
                    this.salesChartLabels.push(res[i].month);
                    let temp_val = parseFloat(res[i].sale_value).toFixed(2);
                    this.salesChartData[0].data.push(temp_val);
                    this.soldAmounts.push(parseFloat(temp_val));
                }

                this.salesChart.labels = this.salesChartLabels;
                this.salesChart.datasets = this.salesChartData;
                this.salesChart.chart.update();

            }, errRes => {
                toastr.error('Something went wrong !');
            });

        } else if(this.salesReportFrequency == "Weekly") {
            this.billingService.getWeeklyReportSales(this.offset, this.limit_rows).subscribe( res=> {
                for (let i = 0; i<res.length; i++){
                    this.salesChartLabels.push("Week " + res[i].week);
                    let temp_val = parseFloat(res[i].sale_value).toFixed(2);
                    this.salesChartData[0].data.push(temp_val);
                    this.soldAmounts.push(parseFloat(temp_val));
                }

                this.salesChart.labels = this.salesChartLabels;
                this.salesChart.datasets = this.salesChartData;
                this.salesChart.chart.update();

            }, errRes => {
                toastr.error('Something went wrong !');
            });

        } else if( this.salesReportFrequency == "Daily"){
            this.billingService.getDailyReportSales(this.offset, this.limit_rows).subscribe( res=> {
                for (let i = 0; i<res.length; i++){
                    this.salesChartLabels.push( "" + res[i].date);
                    let temp_val = parseFloat(res[i].sale_value).toFixed(2);
                    this.salesChartData[0].data.push(temp_val);
                    // this.soldAmounts.push(parseFloat(temp_val));
                }
                this.salesChart.labels = this.salesChartLabels;
                this.salesChart.datasets = this.salesChartData;
                this.salesChart.chart.update();

            }, errRes => {
                toastr.error('Something went wrong !');
            });
        }
        else if( this.salesReportFrequency == "MonthlyItemWise"){
            this.billingService.findMonthlySoldItemsReport(this.pg_month, this.pg_year).subscribe( res=> {
                for (let i = 0; i<res.length; i++){
                    this.salesChartLabels.push( "" + res[i].item_name);
                    let temp_val = parseFloat(res[i].quantity);
                    this.salesChartData[0].data.push(temp_val);
                    // this.soldAmounts.push(parseFloat(temp_val));
                }
                this.salesChart.labels = this.salesChartLabels;
                this.salesChart.datasets = this.salesChartData;
                this.salesChart.chart.update();

            }, errRes => {
                toastr.error('Something went wrong !');
            });
        }
    }

    populateByTime(span : string){
        this.offset = 0; // reset offset
        this.salesReportFrequency = span;
        this.updateSalesChart();
    }


    // Enter stock In typeahead choose Item name
    searchItem = (text$: Observable<string>) =>
        text$
            .debounceTime(200)
            .do((term) => {
                this.itemService.findItemLike({value: term, col : 'name' , category: this.selectedCategory, subCategory: this.selectedSubCategory}).subscribe( res => {

                    for(let i = 0; i < res.length; i++){
                        if(-1 == this.stockInItemNames.findIndex(k => k == res[i].name)){
                            this.items.push(res[i]);
                            this.stockInItemNames.push(res[i].name);
                        }
                    }
                })
            })
            .map(term =>
                this.stockInItemNames.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));


    itemNameSelected(option) {
        for (let i = 0; i < this.items.length ; i++){
            this.selectedItemName = option.item;
            if(this.items[i].name == option.item){
                this.stockInItemPrice = this.items[i].stock_price;
                this.stockInForm.patchValue({
                    'reoder_level' : this.items[i].stock_reorder_level,
                    'itemId' : this.items[i].id
                });
            }
        }
    }

    categorySelected(){
        // ~ resetting form ~ //
        let today = new Date();
        
        this.stockInForm.reset({
            'date_in' : this.datePipe.transform(today, 'yyyy-MM-dd')});
        this.selectedItemName = null;
        this.stockInItemNames = [];

        let chosed_category = this.itemsCategories.find(x => x.name == this.selectedCategory);
        // ~ Fetching item sub categories ~ //
        this.itemService.getSubCategoriesOfCategory(chosed_category.id).subscribe( sub_categories => {
            this.itemSubCategories = sub_categories;
        }, errRes => {
            if(errRes.data[0]){
                toastr.error(errRes.data[0].msg);
            }else {
                toastr.error('Some thing went wrong !');
            }
        });

    }


    calculateValue(event) {
        let quantity = event.target.value;
        this.stockInValue = this.stockInItemPrice * quantity;
        this.stockInForm.patchValue({
           'value': this.stockInValue
        });
    }


    stockInformSubmit(formValue) {

        if(this.stockInValue <= 0 ){
            toastr.error('Input data error');
            return;
        }

        this.stockService.stockIn(formValue).subscribe(res => {
           toastr.success('Registered to stock');
            let today = new Date();
            this.stockInForm.reset({
                'date_in' : this.datePipe.transform(today, 'yyyy-MM-dd')});
            this.stockInValue = 0;
        }, err => {
            toastr.error('Error');
        });

        // ~ update stock details ~ //
        // ~ fetching stock details ~ //
        this.stockService.getStockDetails().subscribe( res => {
            this.itemsInStock = res[0].items_available;
            this.stockValueInBirr = res[0].value_of_stock;
            this.stockValueCalcuated_with_price1_InBirr = res[0].stock_calculated_value;
        }, errRes => {
            toastr.error('Something went wrong !');
        });

        // Update charts
        this.updateStockChart();

        // ~ updating stock details ~ //
        this.stockService.getStockDetails().subscribe( res => {
            this.itemsInStock = res[0].items_available;
            this.stockValueInBirr = res[0].value_of_stock;
            this.stockValueCalcuated_with_price1_InBirr = res[0].stock_calculated_value;
        }, errRes => {
            toastr.error('Something went wrong !');
        });

        // ~ updating stock in log history ~ //
        this.stockService.getStockInHistory().subscribe(res => {
            this.StockIns = res;
        }, err => {
            toastr.error('Unable to fetch stock in history');
        });
    }

    applyFilter(filterValue: string) {
        console.log(filterValue);
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.StockIns.filter = filterValue;
        // this.StockIns = this.StockIns.map(val => {
        //     if(this.StockIns.filterPredicate(val, filterValue)){
        //         console.log(val);
        //         return val;
        //     }
        // } );
        console.log(this.StockIns);
    }

    paginateForward() {

        if( this.salesReportFrequency == "MonthlyItemWise") {
            console.log(this.pg_month + ' ' + this.pg_year);
            if(this.pg_month == 1){
                this.pg_year--;
                this.pg_month = 12;
            }else {
                this.pg_month--;
            }
            this.offset++; // This is a bad implementation I am just doing
            // this because I hv used it in ng if for forward pagination button
        }else {
            this.offset += this.no_rows_to_load;
        }

        this.updateSalesChart();
    }
    paginateBackward() {

        if( this.salesReportFrequency == "MonthlyItemWise") {
            if(this.pg_month == 12){
                this.pg_year++;
                this.pg_month = 1;
            }else {
                this.pg_month++;
            }
        }else {

            this.offset -= this.no_rows_to_load;

        }
        this.updateSalesChart();
    }

    exportStockChartToExcel(){

        if(this.itemsQuantities.length != this.StockChartLabels.length){
            toastr.error('Something went wrong while exporting data !');
            return;
        }

        let exportData : any[] = [];

        for(let i = 0; i < this.StockChartLabels.length; i++){
            exportData.push(
                {
                    'Name' : this.StockChartLabels[i],
                    'Stock Value' : this.itemsQuantities[i]
                }
            )
        }

        let rightNow = new Date();
        let fileName = 'Stock ' + this.stockChartPopulateBy + ' wise report ' + rightNow.toISOString().slice(0,10).replace(/-/g,"/") ;
        new Angular2Csv(exportData, fileName );

    }

    exportSaleChartToExcel(){

        if(this.salesChartData[0].data.length != this.salesChartLabels.length){
            toastr.error('Something went wrong while exporting data !');
            return;
        }

        let exportData : any[] = [];

        for(let i = 0; i < this.salesChartLabels.length; i++){
            exportData.push(
                {
                    'Period' : this.salesChartLabels[i],
                    'Value' : this.salesChartData[0].data[i]
                }
            )
        }

        let rightNow = new Date();
        let fileName = '';
        if(this.salesReportFrequency == 'MonthlyItemWise'){
            // inculde the month and the year information on the exported filename
             fileName = 'Sale ' + this.salesReportFrequency + ' report of Month ' + this.pg_month + ' and Year ' + this.pg_year  ;

        }else {
            fileName = 'Sale ' + this.salesReportFrequency + ' report ' + rightNow.toISOString().slice(0,10).replace(/-/g,"/") ;
        }

        new Angular2Csv(exportData, fileName );

    }

}
